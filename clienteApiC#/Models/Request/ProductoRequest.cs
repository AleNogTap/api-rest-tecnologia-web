﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace aplicacion1.Models.Request
{
    class ProductoRequest
    {
        public int id_categoria { get; set; }
        public int id_imagen { get; set; }
        public int id_usuario { get; set; }
        public string nombre { get; set; }
        public string descripcion { get; set; }
        public int cantidad { get; set; }
        public double precio { get; set; }
        public DateTime fechaCaptura { get; set; }
    }
}
