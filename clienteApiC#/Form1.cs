﻿using aplicacion1.Models.Request;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.IO;
using System.Linq;
using System.Net;
using System.Text;
using System.Threading.Tasks;
using System.Web.Script.Serialization;
using System.Windows.Forms;

namespace aplicacion1
{
    public partial class Form1 : Form
    {
        public Form1()
        {
            InitializeComponent();
        }

        private void button1_Click(object sender, EventArgs e)
        {
            lbRespuesta.Text = "";
            string url = "http://localhost:63992/api/articulos"; //depende el Api y donde se aloje se usa los puertos 63992(.Net) y 8000(Laravel)
            ProductoRequest objProducto = new ProductoRequest();
            objProducto.id_categoria = Int32.Parse(cbxCat.SelectedIndex.ToString());
            objProducto.id_imagen = 1;
            objProducto.id_usuario = 1;
            objProducto.nombre = txtNombre.Text;
            objProducto.descripcion = txtDescript.Text;
            objProducto.cantidad = Int32.Parse(txtCantidad.Text);
            objProducto.precio = Int32.Parse(txtPrecio.Text);
            string resultado = Send<ProductoRequest>(url, objProducto, "POST");
            lbRespuesta.Text = resultado;
        }
        
        private async void button2_Click(object sender, EventArgs e)
        {
            string respuesta = await GetDatos();
            List<ProductoRequest> lst = JsonConvert.DeserializeObject<List<ProductoRequest>>(respuesta);
            dataGridView1.DataSource = lst;
        }

        public async Task<string> GetDatos()
        {
            WebRequest objRequest = WebRequest.Create("http://localhost:63992/api/articulos");
            WebResponse objResponse = objRequest.GetResponse();
            StreamReader cadena = new StreamReader(objResponse.GetResponseStream());
            return await cadena.ReadToEndAsync();

        }


        public string Send<T>(string url, T objectRequest, string method = "POST")
        {
            string result = "";

            try
            {

                JavaScriptSerializer js = new JavaScriptSerializer();

                //serializamos el objeto
                string json = Newtonsoft.Json.JsonConvert.SerializeObject(objectRequest);

                //peticion
                WebRequest request = WebRequest.Create(url);
                //headers
                request.Method = method;
                request.PreAuthenticate = true;
                request.ContentType = "application/json;charset=utf-8'";
                request.Timeout = 10000; //esto es opcional

                using (var streamWriter = new StreamWriter(request.GetRequestStream()))
                {
                    streamWriter.Write(json);
                    streamWriter.Flush();
                }

                var httpResponse = (HttpWebResponse)request.GetResponse();
                using (var streamReader = new StreamReader(httpResponse.GetResponseStream()))
                {
                    result = streamReader.ReadToEnd();
                }

            }
            catch (Exception e)
            {
                result = e.Message;

            }

            return result;
        }
    }
}
