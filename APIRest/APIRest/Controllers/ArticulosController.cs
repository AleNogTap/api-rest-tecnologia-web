﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;
using APIRest.Models;

namespace APIRest.Controllers
{
    public class ArticulosController : ApiController
    {
        [HttpGet]
        public IEnumerable<articulos> Get()
        {
            string strCadena = "Data Source=DESKTOP-SD4KB8T;Initial Catalog=VentasTecWeb2;Integrated Security=True";
            claseConexion rsQ = new claseConexion("select * from articulos", strCadena);
            rsQ.Ejecutar();

            articulos A;
            List<articulos> datos = new List<articulos>();
            foreach (DataRow d in rsQ.DT.Rows)
            {
                A = new articulos();
                A.id_producto = Int32.Parse(d["id_producto"].ToString());
                A.id_categoria = Int32.Parse(d["id_categoria"].ToString());
                A.id_imagen = Int32.Parse(d["id_imagen"].ToString());
                A.id_usuario = Int32.Parse(d["id_usuario"].ToString());
                A.nombre = d["nombre"].ToString();
                A.descripcion = d["descripcion"].ToString();
                A.cantidad = Int32.Parse(d["cantidad"].ToString());
                A.precio = float.Parse(d["precio"].ToString());
                A.fechaCaptura = DateTime.Parse(d["fechaCaptura"].ToString());
                datos.Add(A);
            }
            return datos;
        }

        [HttpGet]
        public IEnumerable<articulos> Get(int id)
        {
            string strCadena = "Data Source=DESKTOP-SD4KB8T;Initial Catalog=VentasTecWeb2;Integrated Security=True";
            claseConexion rsQ = new claseConexion("select * from articulos", strCadena);
            rsQ.Ejecutar();

            articulos A;
            List<articulos> datos = new List<articulos>();
            foreach (DataRow d in rsQ.DT.Rows)
            {
                if (Int32.Parse(d["id_producto"].ToString()) == id)
                {
                    A = new articulos();
                    A.id_producto = Int32.Parse(d["id_producto"].ToString());
                    A.id_categoria = Int32.Parse(d["id_categoria"].ToString());
                    A.id_imagen = Int32.Parse(d["id_imagen"].ToString());
                    A.id_usuario = Int32.Parse(d["id_usuario"].ToString());
                    A.nombre = d["nombre"].ToString();
                    A.descripcion = d["descripcion"].ToString();
                    A.cantidad = Int32.Parse(d["cantidad"].ToString());
                    A.precio = float.Parse(d["precio"].ToString());
                    A.fechaCaptura = DateTime.Parse(d["fechaCaptura"].ToString());
                    datos.Add(A);
                }
            }
            return datos;
        }

        [HttpPost]
        public IHttpActionResult Add(Models.Request.ArticuloRequest modelo)
        {
            using (VentasTecWeb2Entities bd = new VentasTecWeb2Entities())
            {
                try
                {
                    var obj = new articulos();
                    obj.id_producto = modelo.id_producto;
                    obj.id_categoria = modelo.id_categoria;
                    obj.id_imagen = modelo.id_imagen;
                    obj.id_usuario = modelo.id_usuario;
                    obj.nombre = modelo.nombre;
                    obj.descripcion = modelo.descripcion;
                    obj.cantidad = modelo.cantidad;
                    obj.precio = modelo.precio;
                    obj.fechaCaptura = modelo.fechaCaptura;
                    bd.articulos.Add(obj);
                    bd.SaveChanges();
                }
                catch (Exception)
                {
                    throw;
                }
            }
            return Ok("Articulos registrado");
        }
    }
}
