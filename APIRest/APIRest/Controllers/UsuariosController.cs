﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;
using APIRest.Models;

namespace APIRest.Controllers
{
    public class UsuariosController : ApiController
    {
        [HttpGet]
        public IEnumerable<usuarios> Get()
        {
            using (VentasTecWeb2Entities bd = new VentasTecWeb2Entities())
            {
                return bd.usuarios.ToList();
            }
        }

        [HttpGet]
        public IEnumerable<usuarios> Get(int id)
        {
            using (VentasTecWeb2Entities bd = new VentasTecWeb2Entities())
            {
                var obj = bd.usuarios.Where(d => d.id_usuario == id).ToList();
                return obj;
            }
        }

        [HttpGet]
        public IEnumerable<usuarios> Get(string dato)
        {
            using (VentasTecWeb2Entities bd = new VentasTecWeb2Entities())
            {
                var obj = bd.usuarios.Where(d => d.email.Contains(dato)).ToList();
                return obj;
            }
        }

        [HttpPost]
        public IHttpActionResult Add(Models.Request.UsuarioRequest modelo)
        {
            using (VentasTecWeb2Entities bd = new VentasTecWeb2Entities())
            {
                try
                {
                    var obj = new usuarios();
                    obj.id_usuario = modelo.id_usuario;
                    obj.nombre = modelo.nombre;
                    obj.apellido = modelo.apellido;
                    obj.email = modelo.email;
                    obj.password = modelo.password;
                    obj.fechaCaptura = modelo.fechaCaptura;
                    bd.usuarios.Add(obj);
                    bd.SaveChanges();
                }
                catch (Exception)
                {

                    throw;
                }


            }
            return Ok("Usuario registrado");
        }
    }
}
