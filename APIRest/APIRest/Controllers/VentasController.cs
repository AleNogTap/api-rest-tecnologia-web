﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;

namespace APIRest.Controllers
{
    public class VentasController : ApiController
    {
        [HttpGet]
        public IEnumerable<ventas> Get()
        {
            using (VentasTecWeb2Entities bd = new VentasTecWeb2Entities())
            {
                return bd.ventas.ToList();
            }
        }

        [HttpGet]
        public IEnumerable<ventas> Get(int id)
        {
            using (VentasTecWeb2Entities bd = new VentasTecWeb2Entities())
            {
                var obj = bd.ventas.Where(d => d.id_venta == id).ToList();
                return obj;
            }
        }

        [HttpPost]
        public IHttpActionResult Add(Models.Request.VentaRequest modelo)
        {
            using (VentasTecWeb2Entities bd = new VentasTecWeb2Entities())
            {
                try
                {
                    var obj = new ventas();
                    obj.id_venta = modelo.id_venta;
                    obj.id_cliente = modelo.id_cliente;
                    obj.id_producto = modelo.id_producto;
                    obj.id_usuario = modelo.id_usuario;
                    obj.precio = modelo.precio;
                    obj.fechaCompra = modelo.fechaCompra;
                    bd.ventas.Add(obj);
                    bd.SaveChanges();
                }
                catch (Exception)
                {

                    throw;
                }


            }
            return Ok("Categoria registrada");
        }
    }
}
}
