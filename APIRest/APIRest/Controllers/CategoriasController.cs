﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;
using APIRest.Models;

namespace APIRest.Controllers
{
    public class CategoriasController : ApiController
    {
        [HttpGet]
        public IEnumerable<categorias> Get()
        {
            using (VentasTecWeb2Entities bd = new VentasTecWeb2Entities())
            {
                return bd.categorias.ToList();
            }
        }

        [HttpGet]
        public IEnumerable<categorias> Get(int id)
        {
            using (VentasTecWeb2Entities bd = new VentasTecWeb2Entities())
            {
                var obj = bd.categorias.Where(d => d.id_categoria == id).ToList();
                return obj;
            }
        }

        [HttpGet]
        public IEnumerable<categorias> Get(string dato)
        {
            using (VentasTecWeb2Entities bd = new VentasTecWeb2Entities())
            {
                var obj = bd.categorias.Where(d => d.nombreCategoria.Contains(dato)).ToList();
                return obj;
            }
        }

        [HttpPost]
        public IHttpActionResult Add(Models.Request.CategoriaRequest modelo)
        {
            using (VentasTecWeb2Entities bd = new VentasTecWeb2Entities())
            {
                try
                {
                    var obj = new categorias();
                    obj.id_categoria = modelo.id_categoria;
                    obj.id_usuario = modelo.id_usuario;
                    obj.nombreCategoria = modelo.nombreCategoria;
                    obj.fechaCaptura = modelo.fechaCaptura;
                    bd.categorias.Add(obj);
                    bd.SaveChanges();
                }
                catch (Exception)
                {

                    throw;
                }


            }
            return Ok("Categoria registrada");
        }
    }
}
