﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;
using APIRest.Models;

namespace APIRest.Controllers
{
    public class ClientesController : ApiController
    {
        [HttpGet]
        public IEnumerable<clientes> Get()
        {
            using (VentasTecWeb2Entities bd = new VentasTecWeb2Entities())
            {
                return bd.clientes.ToList();
            }
        }

        [HttpGet]
        public IEnumerable<clientes> Get(int id)
        {
            using (VentasTecWeb2Entities bd = new VentasTecWeb2Entities())
            {
                var obj = bd.clientes.Where(d => d.id_cliente == id).ToList();
                return obj;
            }
        }

        [HttpGet]
        public IEnumerable<clientes> Get(string dato)
        {
            using (VentasTecWeb2Entities bd = new VentasTecWeb2Entities())
            {
                var obj = bd.clientes.Where(d => d.ci.Contains(dato)).ToList();
                return obj;
            }
        }

        [HttpPost]
        public IHttpActionResult Add(Models.Request.ClienteRequest modelo)
        {
            using (VentasTecWeb2Entities bd = new VentasTecWeb2Entities())
            {
                try
                {
                    var obj = new clientes();
                    obj.id_cliente = modelo.id_cliente;
                    obj.id_usuario = modelo.id_usuario;
                    obj.nombre = modelo.nombre;
                    obj.apellido = modelo.apellido;
                    obj.direccion = modelo.direccion;
                    obj.email = modelo.email;
                    obj.telefono = modelo.telefono;
                    obj.ci = modelo.ci;
                    bd.clientes.Add(obj);
                    bd.SaveChanges();
                }
                catch (Exception)
                {

                    throw;
                }


            }
            return Ok("Cliente registrado");
        }
    }
}
