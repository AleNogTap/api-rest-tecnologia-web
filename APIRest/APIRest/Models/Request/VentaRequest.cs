﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace APIRest.Models.Request
{
    public class VentaRequest
    {
        public int id_venta { get; set; }
        public int id_cliente { get; set; }
        public int id_producto { get; set; }
        public int id_usuario { get; set; }
        public double precio { get; set; }
        public DateTime fechaCompra { get; set; }

        public virtual articulos articulos { get; set; }
        public virtual clientes clientes { get; set; }
        public virtual usuarios usuarios { get; set; }
    }
}