﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace APIRest.Models.Request
{
    public class ClienteRequest
    {
        public int id_cliente { get; set; }
        public int id_usuario { get; set; }
        public string nombre { get; set; }
        public string apellido { get; set; }
        public string direccion { get; set; }
        public string email { get; set; }
        public string telefono { get; set; }
        public int ci { get; set; }
        public virtual usuarios usuarios { get; set; }
        public virtual ICollection<ventas> ventas { get; set; }
    }
}