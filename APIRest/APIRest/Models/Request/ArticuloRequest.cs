﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace APIRest.Models.Request
{
    public class ArticuloRequest
    {
        public int id_producto { get; set; }
        public int id_categoria { get; set; }
        public int id_imagen { get; set; }
        public int id_usuario { get; set; }
        public string nombre { get; set; }
        public string descripcion { get; set; }
        public int cantidad { get; set; }
        public double precio { get; set; }
        public DateTime fechaCaptura { get; set; }
        public virtual categorias categorias { get; set; }
        public virtual usuarios usuarios { get; set; }
        public virtual ICollection<ventas> ventas { get; set; }
    }
}