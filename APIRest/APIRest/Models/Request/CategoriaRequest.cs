﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace APIRest.Models.Request
{
    public class CategoriaRequest
    {
        public int id_categoria { get; set; }
        public int id_usuario { get; set; }
        public string nombreCategoria { get; set; }
        public DateTime fechaCaptura { get; set; }
        public virtual ICollection<articulos> articulos { get; set; }
        public virtual usuarios usuarios { get; set; }
    }
}