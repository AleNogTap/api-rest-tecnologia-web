﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace APIRest.Models.Request
{
    public class UsuarioRequest
    {
        public int id_usuario { get; set; }
        public string nombre { get; set; }
        public string apellido { get; set; }
        public string email { get; set; }
        public string password { get; set; }
        public DateTime fechaCaptura { get; set; }
        public virtual ICollection<articulos> articulos { get; set; }
        public virtual ICollection<categorias> categorias { get; set; }
        public virtual ICollection<clientes> clientes { get; set; }
        public virtual ICollection<ventas> ventas { get; set; }
    }
}