use master
create database VentasTecWeb2
use VentasTecWeb2

create table usuarios(
				id_usuario int identity (1,1),
				nombre nvarchar(50),
				apellido nvarchar(50),
				email nvarchar(50),
				password nvarchar(50),
				fechaCaptura date,
				primary key(id_usuario)
					);

create table categorias (
				id_categoria int identity (1,1),
				id_usuario int not null,
				nombreCategoria nvarchar(150),
				fechaCaptura date,
				primary key(id_categoria),
				foreign key (id_usuario) references usuarios(id_usuario)
						);


-- No se sabe si se usara aun
create table imagenes(
				id_imagen int identity(1,1),
				id_categoria int not null,
				nombre nvarchar(500),
				ruta nvarchar(500),
				fechaSubida date,
				primary key(id_imagen)
					);
create table articulos (
				id_producto int identity(1,1),
				id_categoria int not null,
				id_imagen int not null,
				id_usuario int not null,
				nombre nvarchar(50),
				descripcion nvarchar(500),
				cantidad int,
				precio float,
				fechaCaptura date,
				primary key(id_producto),
				foreign key (id_categoria) references categorias(id_categoria),
				foreign key (id_usuario) references usuarios(id_usuario)
						);

create table clientes (
				id_cliente int identity(1,1),
				id_usuario int not null,
				nombre nvarchar(100),
				apellido nvarchar(100),
				direccion nvarchar(100),
				email nvarchar(100),
				telefono nvarchar(12),
				ci int,
				primary key(id_cliente),
				foreign key (id_usuario) references usuarios(id_usuario)
					);

create table ventas (
				id_venta int identity(1,1),
				id_cliente int,
				id_producto int,
				id_usuario int,
				precio float,
				fechaCompra date,
				foreign key (id_cliente) references clientes(id_cliente),
				foreign key (id_producto) references articulos(id_producto),
				foreign key (id_usuario) references usuarios(id_usuario)
				);